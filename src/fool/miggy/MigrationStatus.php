<?php


namespace fool\miggy;

/**
 * When migrations are discovered they get a status. This indicates what
 * the current state of the migration is and helps decide what needs to be
 * done with it.
 */
class MigrationStatus
{
    /**
     * The migration is missing from database
     * @var int
     */
    const STATUS_MISSING  = 0;

    /**
     * The migration exists in the database
     * @var int
     */
    const STATUS_GOOD     = 1;

    /**
     * There was an error with this migration
     * @var int
     */
    const STATUS_ERROR    = 2;

    /**
     * Map of status ids to names and reverse map
     * @var array
     */
    private static $statusDefinitions = array(
        'missing'  => self::STATUS_MISSING,
        'good'    => self::STATUS_GOOD,
        'error' => self::STATUS_ERROR,

        self::STATUS_MISSING  => 'missing',
        self::STATUS_GOOD     => 'good',
        self::STATUS_ERROR    => 'error',
    );


    /**
     * Migration is not guaranteed to be defined. The status should have enough information to tell
     *
     * @var Migration|null
     */
    private $migration;

    /**
     * One of the status ids
     * @var int
     */
    private $status;

    /**
     * @param Migration|null $migration
     * @param int $status
     */
    function __construct(Migration $migration = null, $status)
    {
        $this->migration = $migration;
        $this->status = $status;
    }

    /**
     * @return Migration|null
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPrintableStatus()
    {
        return self::$statusDefinitions[$this->status];
    }

    /**
     * @return string
     */
    public function toString()
    {
        if ($this->migration) {
            $migrationString = $this->migration;
        } else {
            $migrationString = 'missing';
        }
        return sprintf("%s\t%s", $this->getPrintableStatus(), $migrationString);
    }
}
