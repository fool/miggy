<?php

namespace fool\miggy\impl;
use \fool\miggy\Adapter;
use \fool\miggy\Migration;
use \Psr\Log\LoggerInterface;
use \fool\echolog\Echolog;

/**
 * Default implementation of the miggy adapter. Designed to be extendable by overriding
 * the getter calls in your subclass.
 *
 * @package fool\miggy\impl
 */
abstract class MiggyAdapter implements Adapter
{
    /**
     * Default time format used in class names.
     * @var string
     */
    const DEFAULT_TIME_FORMAT = "Y_m_d";

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log = null)
    {
        if (!$log) {
            $log = new Echolog();
        }
        $this->log = $log;
    }

    /**
     * Converts a unix timestamp to a human readable time. You can just keep it unix timestamps but it's a little hard
     * to read. By default this is used to construct the class name.
     *
     * @param  int $time unix timestamp
     * @return string
     */
    protected function serializeTime($time)
    {
        return date(self::DEFAULT_TIME_FORMAT, $time);
    }

    /**
     * Creates a migration instance. This finds the subclass of Migration associated with these two unique pairs
     * and instantiates it. This does not include or require any PHP files, your class loader must be able to find
     * the file by fully qualified classname. This is to help you when you are getting all migrations in the database,
     * your table only needs to store the name and time and that will be enough to get a Migration instance.
     *
     * @param  string $name
     * @param  int    $time
     * @return Migration
     */
    public function createMigration($name, $time)
    {
        $className = $this->getMigrationClassName($name, $time);
        $namespace = $this->getNamespace();

        $class = "$namespace\\$className";
        return new $class();
    }

    /**
     * @return \fool\miggy\MigrationCodeGenerator
     */
    public function getMigrationCodeGenerator()
    {
        return new MiggyMigrationCodeGenerator($this->log);
    }

    /**
     * You need to implement a way for miggy to find your previous migrations.
     *
     * @return Migration[]
     */
    abstract public function getAllMigrationsFromDatabase();

    /**
     * Gets the path to the location on disk where migration classes will live. This is used to write
     * newly generated migrations, and discover the migrations in the codebase.
     *
     * @return string
     */
    abstract public function getMigrationDirectory();

    /**
     * Called when a migration has been up()'d. You need to store the migration in the database.
     *
     * @param  Migration $migration
     * @return mixed
     */
    abstract public function writeMigrationToDatabase(Migration $migration);


    /**
     * Generates a migration class name. Here's the reasoning on how the default is formed:
     * - Starts with M because all class names must begin with a letter
     * - Follows with a date time format. This helps the migrations become sortable on disk and also allows for
     *   migrations of the same name to be completed.
     * - Follows with underscore, which is one of the few allowed special characters in class name
     * - Followed by user's name for the migration
     *
     * @param  string  $name  The migration name
     * @param  int     $time  Unix timestamp for this migration
     * @return string
     */
    public function getMigrationClassName($name, $time)
    {
        $serializedTime = $this->serializeTime($time);
        return "M{$serializedTime}_$name";
    }

    /**
     * Default: No namespace is used on auto-generated Migration classes
     *
     * @return string
     */
    public function getNamespace()
    {
        return "";
    }

    /**
     * Called whenever miggy is shutting down. This gives you an opportunity to close your database connections
     * or clean up any state you may have initialized in the adapter. Certain events like Fatal Errors will not
     * ensure shutdown() is called.
     */
    public function shutdown() {}

    /**
     * @return LoggerInterface
     */
    public function getLog()
    {
        return $this->log;
    }
}
