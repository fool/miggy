<?php

namespace fool\miggy\impl;

use fool\miggy\Migration;

/**
 * The parent class of all db migrations
 *
 * @package fool\miggy\impl
 */
abstract class MiggyMigration implements Migration
{
    /**
     * @var string
     */
    private $name;

    /**
     * unix timestamp
     *
     * @var int
     */
    private $time;

    /**
     * @param string $name  the migration name
     * @param int    $time  unix timestamp
     */
    public function __construct($name, $time)
    {
        $this->name = $name;
        $this->time = $time;
    }

    /**
     * Runs the migrations
     */
    abstract public function up();

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Time comparison for sorting.
     * -1 = First migration happens before the second one
     *  0 = The times are the same
     *  1 = Second migration happens before the first one
     *
     *
     * @param  Migration $first
     * @param  Migration $second
     * @return int
     */
    public static function compareTime(Migration $first, Migration $second)
    {
        return $first->getTime() - $second->getTime();
    }

    /**
     * Are these migrations the same?
     *
     * @param  Migration $other
     * @return bool
     */
    public function equals(Migration $other)
    {
        return $this->name === $other->getName() && $this->time === $other->getTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTime() . "\t" . $this->getName();
    }
}
