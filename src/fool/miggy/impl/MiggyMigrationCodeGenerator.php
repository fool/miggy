<?php


namespace fool\miggy\impl;
use \fool\miggy\FileExistsException;
use \fool\miggy\IOException;
use fool\miggy\MigrationCodeGenerator;
use Psr\Log\LoggerInterface;

/**
 * Allows customizable features by subclassing this and overriding the getter methods.
 *
 * @package fool\miggy\impl
 */
class MiggyMigrationCodeGenerator implements MigrationCodeGenerator
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param  string $name
     * @param  int    $time
     * @param  string $className
     * @param  string $namespace
     * @param  string $directory
     * @throws FileExistsException
     * @throws IOException
     * @see MigrationCodeGenerator::writeMigrationFile()
     */
    public function writeMigrationFile($name, $time, $className, $namespace, $directory)
    {
        $useMap = function($thing) {
            return "use $thing;";
        };

        /* take the list of traits and map them into a string we can paste in the file */
        $includedTraits = implode(PHP_EOL, array_map($useMap, $this->getMigrationTraits()));
        $importedClasses = implode(PHP_EOL, array_map($useMap, $this->getImportedClasses()));

        $parentClassName = $this->getMigrationParentClass();
        $parentClassNamespace = $this->getMigrationParentClassNamespace();
        $upBody = $this->getUpBody();

        $fileName = $directory . DIRECTORY_SEPARATOR . $className . ".php";
        $namespace = strlen($namespace) === 0 ? "" : "namespace $namespace;";
        $parentClassNamespace = $parentClassNamespace ? "use $parentClassNamespace;" : '';

        /* nitpick: $includedTraits should be indented only if there are traits. this prevents unnecessary
         * whitespaces errors in IDEs/Git/Static Analysis tools. */
        if ($includedTraits) {
            $includedTraits = PHP_EOL . '    ' . $includedTraits;
        }
        /* same for $upBody */
        if ($upBody) {
            $upBody = '        ' . $upBody;
        }

        $fileContents = <<<CONTENTS
<?php

{$namespace}
{$parentClassNamespace}
{$importedClasses}

class $className extends $parentClassName
{{$includedTraits}
    public function __construct()
    {
        parent::__construct('{$name}', {$time});
    }

    public function up()
    {
{$upBody}
    }
}
CONTENTS;

        if (file_exists($fileName)) {
            throw new FileExistsException($fileName);
        }
        $this->writeFile($fileName, $fileContents);

    }

    /**
     * Writes the contents to disk. Throws exceptions on errors.
     *
     * @param string $file
     * @param string $contents
     * @throws IOException
     */
    protected function writeFile($file, $contents)
    {
        $parentDirectory = dirname($file);
        if (!is_dir($parentDirectory)) {
            if (!mkdir($parentDirectory, 0755, true)) {
                $error = error_get_last();
                throw new IOException("Destination file is in a directory that does not exist: $file; failed creating parent direcrtory", $error);
            }
        }

        $this->log->info("Writing $file");
        $bytesWritten = file_put_contents($file, $contents);
        if ($bytesWritten === false || $bytesWritten !== strlen($contents)) {
            $error = error_get_last();
            throw new IOException("Error write to $file", $error);
        }
    }


    /**
     * A list of traits that will be included in any auto-generated migration files.
     * This is an easy way to introduce code sharing across migrations. Each trait should
     * be a fully qualified class name.
     *
     * @return string[]
     */
    public function getMigrationTraits()
    {
        return array();
    }

    /**
     * A list of classes that will be included in any auto-generated migration files.
     * These are added at the top under the namespace declaration with 'use' in front of it.
     *
     * @return string[]
     */
    public function getImportedClasses()
    {
        return array();
    }

    /**
     * The class all generated migrations will inherit from
     * @return string
     */
    public function getMigrationParentClass()
    {
        return 'MiggyMigration';
    }

    /**
     * The namespace to import for the parent class. This can be
     * empty if the parent class is not in a namespace.
     *
     * @return string
     */
    public function getMigrationParentClassNamespace()
    {
        return '\fool\miggy\impl\MiggyMigration';
    }

    /**
     * The default body of the up method
     * @return string
     */
    public function getUpBody()
    {
        return '// code here bro';
    }
}
