<?php

namespace fool\miggy\impl;
use \fool\miggy\Adapter;
use \fool\miggy\CodeGenerationException;
use \fool\miggy\Migrator;
use \fool\miggy\Migration;
use \fool\miggy\MigrationStatus;

/**
 * A program that does migration related tasks.
 * Can help you create migrations, show status, and run them.
 * There is only migrate up, no down.
 *
 *
 * To use this you need to subclass MiggyAdapter. Then you can create and run the migrator executable:
 *
 *
 * $myAdapter = new MyMiggyAdapter();
 * $migrator = new Miggy($myAdapter);
 * $migrator->run();
 */
class Miggy implements Migrator
{
    const VERSION = '2.1.1';

    /* miggy modes of operation */
    const NO_ACTION = -1;
    const CREATE    = 0;
    const UP        = 1;
    const TEST      = 2;
    const LISTING   = 4;
    const SKIP      = 5;
    const RUN       = 6;

    /* for identifying where migrations exist */
    const DISK = 0;
    const DB   = 1;
    const BOTH = 2;

    const UNLIMITED = -1;

    /**
     * Used in the Create action
     * @var string
     */
    private $fileName;

    /**
     * Used in the Skip action
     * @var string
     */
    private $skipMigration;

    /**
     * Used in the Skip action
     * @var string
     */
    private $runMigration;

    /**
     * One of the defined actions above
     * @var int
     */
    private $action = self::NO_ACTION;

    /**
     * @var string
     */
    private $migrationDir;

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $log;

    /**
     * When running migrations, how many are allowed to be run.
     * @var int
     */
    private $limit;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->log = $adapter->getLog();
        $this->migrationDir = $adapter->getMigrationDirectory();
        $this->limit = self::UNLIMITED;
    }

    /**
     * Parses the command line args
     */
    protected function parseCommandLineArguments()
    {
        $options = $this->getOpt();
        if (isset($options['create'])) {
            $this->fileName = $options['create'];
            $this->action = self::CREATE;
        } else if (isset($options['up'])) {
            $this->action = self::UP;
        } else if (isset($options['test'])) {
            $this->action = self::TEST;
        } else if (isset($options['list'])) {
            $this->action = self::LISTING;
        } else if (isset($options['skip'])) {
            $this->action = self::SKIP;
            $this->skipMigration = $options['skip'];
        } else if (isset($options['run'])) {
            $this->action = self::RUN;
            $this->runMigration = $options['run'];
        }
        if (isset($options['limit'])) {
            $limit = intval($options['limit']);

            if ($limit <= 0) {
                $this->log->error("Invalid limit specified");
                $this->action = self::NO_ACTION;
            } else {
                $this->limit = $limit;
                $this->log->info("Only migrating up {$this->limit} migrations");
            }
        }
    }

    /**
     * Prints help
     */
    public function printHelp()
    {
        $version = self::VERSION;
        $help = <<<MESSAGE
Miggy v$version
Usage:  miggy [mode] [options]

MODES
--create  <name_of_migration>    create a skeleton PHP file to do a migration in

--help, --?                      show this

--list                           show all found migrations

--run     <name_of_migration>    run a single migration, can be any missing migration

--skip    <name_of_migration>    mark this migration as done without running it

--test                           test migrate everything to the most up to date
                                 only prints what would happen, does not do anything

--up                             migrate everything to the most up to date. always migrates
                                 the earliest missing migration in order to the most recent

OPTIONS
--limit   <count>                migrate up this many migrations, then stop. default is unlimited.
                                 this only works with --up and --test

MESSAGE;

        echo $help, PHP_EOL;
    }

    /**
     * This is intended to be used as an executable program.
     */
    public function run()
    {
        $this->parseCommandLineArguments();
        $success = true;
        switch($this->action) {
            case self::CREATE:
                $success = $this->create($this->fileName);
                break;
            case self::LISTING:
                $this->listAllMigrations();
                break;
            case self::RUN:
                $this->runMigration($this->runMigration);
                break;
            case self::SKIP:
                $this->skipMigration($this->skipMigration);
                break;
            case self::TEST:
                $this->migrateUp(true);
                break;
            case self::UP:
                $this->migrateUp();
                break;
            default:
                $this->printHelp();
        }
        $this->adapter->shutdown();

        if (!$success) {
            exit(3);
        }
    }

    /**
     * Find all missing migrations and run them
     *
     * @param bool $noop   True if no migrations should actually be run, just print what would have happened
     */
    public function migrateUp($noop = false)
    {
        $migrationStatuses = $this->getDifference();
        $migrationsRun = 0;
        $this->log->info("Found " . count($migrationStatuses) . " migrations");
        foreach ($migrationStatuses as $migrationStatus) {
            $status = $migrationStatus->getStatus();
            $migration = $migrationStatus->getMigration();
            $name = $this->getMigrationName($migration);
            switch ($status) {
                case MigrationStatus::STATUS_GOOD:
                    $this->log->info("GOOD: $name");
                    break;

                case MigrationStatus::STATUS_MISSING:
                    $this->upMigration($noop, $migration);
                    $migrationsRun += 1;
                    break;

                case MigrationStatus::STATUS_ERROR:
                    $this->log->error("Invalid migration $name");
                    break;

                default:
                    $this->log->critical("Unknown error occured");
                    break;
            }

            if ($this->hasReachedMigrationLimit($migrationsRun)) {
                $this->log->info("Migration limit reached");
                break;
            }
        }
    }

    /**
     * @param  int $migrationsRun  Count of how many migrations have been run so far
     * @return bool True if the migration limit has been reached and no more migrations should be run, False other wise
     */
    protected function hasReachedMigrationLimit($migrationsRun)
    {
        return $this->limit !== self::UNLIMITED
            && $migrationsRun >= $this->limit;
    }

    /**
     * @param bool      $noop
     * @param Migration $migration
     */
    protected function upMigration($noop, Migration $migration)
    {
        $name = $migration->getName();
        if ($noop) {
            $this->log->info("Would have migrated up $name");
        } else {
            $this->log->info("Migrating up $name");
            $migration->up();
            $this->adapter->writeMigrationToDatabase($migration);
        }
    }

    /**
     * Helper to get the name, the only reason this is needed is to keep serializeTime() protected
     * on the adapter class.
     *
     * @param  Migration $migration
     * @return string
     */
    protected function getMigrationName(Migration $migration)
    {
        return $this->adapter->getMigrationClassName($migration->getName(), $migration->getTime());
    }

    /**
     * Gets a list of all migrations and has the status of each one, which can be
     * - good      migration exists in both db and disk
     * - missing   migration exists in disk only
     * - error     migration has some error
     *
     * @return MigrationStatus[]
     */
    public function getDifference()
    {
        $allMigrations = $this->getAllMigrationsAsTypedMigration();

        /* sort the big list of migrations by time */
        $sortSuccess = usort($allMigrations, array('\fool\miggy\impl\TypedMigration', 'compare'));
        if (!$sortSuccess) {
            $this->log->alert('Unable to sort migrations');
        }

        /* loop over the list and identify pairs. there should be 2 migrations with identical values next to
         * each other. If you only find one, then there is a missing migration
         */
        $max = count($allMigrations);
        $migrationStatus = array();
        for ($i = 0; $i < $max; $i++) {
            $a = $allMigrations[$i];
            if ($i + 1 < $max) {
                $b = $allMigrations[$i + 1];
            } else {
                $b = null;
            }

            $aMigration = $a->getMigration();
            $bMigration = $b ? $b->getMigration() : null;

            $aType = $a->getType();
            $bType = $b ? $b->getType() : null;

            /* if there is a $b, check if it's equal */
            if ($b) {
                if ($aMigration->equals($bMigration) && $aType !== $bType) {
                    $migrationStatus[] = new MigrationStatus($aMigration, MigrationStatus::STATUS_GOOD);
                    /* if A and B were side by side matches, increment again so we skip the next iteration,
                     * allowing us to work on 2 migrations at once */
                    $i++;
                    continue;
                }
            }
            /* the migrations are not equal, conflict resolution */

            /* $a is a disk migration and b does not exist or b is also a disk migration. This means
             * $a is missing a corresponding database migration */
            $twoDisksInARow     = ($aType === self::DISK && (!$b || ($bType === self::DISK)));
            $twoDatabasesInARow = ($aType === self::DB   && (!$b || ($bType === self::DB)));

            if ($twoDisksInARow) {
                $migrationStatus[] = new MigrationStatus($aMigration, MigrationStatus::STATUS_MISSING);
            } elseif ($twoDatabasesInARow) {
                $this->log->error("Two database migriations in a row found, unable to continue. $aMigration $bMigration");
                $migrationStatus[] = new MigrationStatus($aMigration, MigrationStatus::STATUS_ERROR);
            } else {
                /* this is not solveable automatically. $a and $b are both defined and different types.
                 * one is DB, one is Disk, and they refer to different things. $b will be processed
                 * again on the next loop, so only mark $a as invalid.
                 */
                $this->log->error("Invalid migration status detected. Migration: " . var_export($a, true));
                $migrationStatus[] = new MigrationStatus($aMigration, MigrationStatus::STATUS_ERROR);
            }
        }
        return $migrationStatus;
    }

    /**
     * @return TypedMigration[]
     */
    private function getAllMigrationsAsTypedMigration()
    {
        $dbMigrations = $this->getAllMigrationsFromDatabase();
        $diskMigrations = $this->getAllMigrationsFromDisk();

        $typedMigrations = array();
        foreach ($diskMigrations as $diskMigration) {
            $typedMigrations[] = new TypedMigration($diskMigration, self::DISK);
        }
        foreach ($dbMigrations as $dbMigration) {
            $typedMigrations[] = new TypedMigration($dbMigration, self::DB);
        }
        return $typedMigrations;
    }

    /**
     * Looks at every file on disk and instantiates them.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDisk()
    {
        $migrations = array();
        $files = scandir($this->migrationDir, 1);
        $namespace = $this->adapter->getNamespace();
        foreach ($files as $file) {
            if ($file[0] === '.') {
                continue;
            }

            $class = $this->getClassNameFromFile($this->migrationDir . DIRECTORY_SEPARATOR . $file);
            if (!$class) {
                $this->log->warning("No migration class located in file: $file. skipping...");
                continue;
            }
            $filename = pathinfo($file, PATHINFO_FILENAME);
            $className = "$namespace\\$filename";
            $migrations[] = new $className;
        }
        if (!usort($migrations, array('fool\miggy\impl\MiggyMigration', 'compareTime'))) {
            $this->log->alert("Unable to sort migrations");
        }


        if ($this->detectDuplicateMigrations($migrations, 'disk')) {
            $this->log->alert("Duplicate disk migrations detected");
        }

        return $migrations;
    }

    /**
     * @param  Migration[] $migrations      A list of migrations sorted by time ascending
     * @param  string      $type            Where did these migrations come from?
     * @return bool  True if there are duplicate migrations, False otherwise
     */
    protected function detectDuplicateMigrations(array $migrations, $type)
    {
        /* compare if any migrations are equal. this is a double for loop so n^2, but it's actually
         * much fewer comparisons since the list is already sorted. In the best case of all unique
         * migrations it's O(n). Each additional duplicate beyond 0 adds 1 extra comparison
         */
        $max = count($migrations);
        $duplicatesFound = false;
        for ($i = 0; $i < $max; $i++) {
            $migration = $migrations[$i];
            if ($i + 1 !== $max) {
                for ($j = $i + 1; $j < $max; $j++) {
                    $comparator = $migrations[$j];
                    if ($migration->equals($comparator)) {
                        $this->log->error(sprintf("Duplicate $type migrations detected: Name: %s Time: %s", $migration->getName(), $migration->getTime()));
                        $duplicatesFound = true;
                    } else {
                        /* they are sorted, so if the next one doesnt match no further ones will match */
                        break;
                    }
                }
            }
        }
        return $duplicatesFound;
    }

    /**
     * Looks at a file and pulls out the first class name it can find.
     *
     * http://stackoverflow.com/questions/7153000/get-class-name-from-file
     * @param  string $filename
     * @return string The name of the first class declared in the file, if found. Empty string if not found.
     */
    protected function getClassNameFromFile($filename)
    {
        $class = '';
        $contents = file_get_contents($filename);
        if ($contents === false) {
            $error = error_get_last();
            $this->log->error("Unable to open php file: $filename: {$error['file']}:{$error['line']} {$error['type']} {$error['message']}");
        } else {
            $tokens = token_get_all($contents);
            $max = count($tokens);
            for ($i = 0; $i < $max; $i++) {
                if ($tokens[$i][0] === T_CLASS) {
                    for ($j = $i + 1; $j < $max; $j++) {
                        if ($tokens[$j] === '{') {
                            $class = $tokens[$i + 2][1];
                            break 2;
                        }
                    }
                }
            }
        }
        return $class;
    }

    /**
     * Gets a sorted list of all migrations from the database with earliest first.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDatabase()
    {
        $migrations = $this->adapter->getAllMigrationsFromDatabase();
        if (!usort($migrations, array('fool\miggy\impl\MiggyMigration', 'compareTime'))) {
            $this->log->alert('Unable to sort migrations');
        }
        if ($this->detectDuplicateMigrations($migrations, 'database')) {
            $this->log->alert("Duplicate datbase migrations detected");
        }
        return $migrations;
    }

    /**
     * Lists all migrations with their statuses. Good migration only show a status of '+' to
     * give the bad (error) statuses more significant visually.
     */
    public function listAllMigrations()
    {
        $status = $this->getDifference();

        $stats = array(
            MigrationStatus::STATUS_MISSING => 0,
            MigrationStatus::STATUS_GOOD => 0,
            MigrationStatus::STATUS_ERROR => 0,
        );

        $this->log->info("Status\tName");
        $this->log->info("-------------------------------------------------------------");
        foreach ($status as $migrationStatus) {
            $statusValue = $migrationStatus->getStatus();
            $stats[$statusValue] += 1;
            if ($statusValue === MigrationStatus::STATUS_GOOD) {
                $status = "+"; /* do not display anything so the bad statuses stand out more */
            } else {
                $status = $migrationStatus->getPrintableStatus();
            }

            $migration = $migrationStatus->getMigration();
            $name = $this->adapter->getMigrationClassName($migration->getName(), $migration->getTime());
            $this->log->info(sprintf("%s\t%s", $status, $name));
        }

        $missingCount = $stats[MigrationStatus::STATUS_MISSING];
        $errorCount   = $stats[MigrationStatus::STATUS_ERROR];
        $this->log->info("$missingCount\tmigrations are missing from database");
        $this->log->info("$errorCount\tmigrations have errors");
    }

    /**
     * Creates a new migration with specified name.
     *
     * @param  string $name  The name of the new migration
     * @param  int    $time  The time this migration is created in UTC timestamp. 0 = use current time (default)
     * @return bool   True on success, False otherwise
     */
    public function create($name, $time = 0)
    {
        if (!$time) {
            $time = time();
        }
        $className = $this->adapter->getMigrationClassName($name, $time);
        $directory = $this->adapter->getMigrationDirectory();
        $generator = $this->adapter->getMigrationCodeGenerator();
        $namespace = $this->adapter->getNamespace();

        $success = false;
        try {
            $generator->writeMigrationFile($name, $time, $className, $namespace, $directory);
            $success = true;
        } catch (CodeGenerationException $exception) {
            $this->log->error($exception->getMessage());
            $this->log->debug($exception->getTraceAsString());
        }
        return $success;
    }

    /**
     * Writes the migration to the migration table - marking this as done - without running the up() function.
     * This is useful if you have an already applied migration and just need miggy to recognize it.
     *
     * @param  string $name
     * @return bool   True if the migration was skipped, False otherwise
     */
    public function skipMigration($name)
    {
        $diskMigrations = $this->getAllMigrationsFromDisk();
        $migrations = array_filter($diskMigrations, $this->filterMigrationByClassName($name));
        $wasSkipped = false;

        if ($migrations) {
            $migration = array_shift($migrations);
            $this->adapter->writeMigrationToDatabase($migration);
            $wasSkipped = true;
            $this->log->info("Skipped migration $name");
        } else {
            $this->log->warning("Unable to find migration $name");
            $this->log->info("No migrations have been skipped.");
        }
        return $wasSkipped;
    }

    /**
     * Creates a closure to filter migrations by class name
     *
     * @param  string $name
     * @return callable
     */
    protected function filterMigrationByClassName($name)
    {
        return function(Migration $migration) use ($name) {
            $className = $this->adapter->getMigrationClassName($migration->getName(), $migration->getTime());
            return $className === $name;
        };
    }

    /**
     * Runs a migration by name. This is useful if you want to run a migration that is not the most recent.
     * up() will always start at the earliest.
     *
     * @param  string $name The name of the migration to run
     * @return bool         True if the migration was run, false otherwise
     */
    public function runMigration($name)
    {
        $diskMigrations = $this->getAllMigrationsFromDisk();
        $migrations = array_filter($diskMigrations, $this->filterMigrationByClassName($name));

        $run = false;
        if ($migrations) {
            $migration = array_shift($migrations);
            $this->upMigration(false, $migration);
            $run = true;
        } else {
            $this->log->warning("Migration not found: $name");
            $this->log->info("No migrations have been run");
        }
        return $run;
    }


    /**
     * uses php's getopt to parse options into an array
     * If an option is found the key will be set. The value is determined by getopt(). This is weird for boolean
     * switches because PHP initializes them to false.
     *
     * @return array
     */
    private function getOpt()
    {
        $options = array(
            'up'      => array('u',  'up'),
            'test'    => array('t',  'test'),
            'create'  => array('c:', 'create:'),
            'list'    => array('l',  'list'),
            'help'    => array('h',  'help'),
            'skip'    => array('s:', 'skip:'),
            'limit'   => array('i:', 'limit:'),
            'run'     => array('r:', 'run:'),
        );
        $shortOptions = implode('', array_map(function($option) {
            return $option[0];
        }, $options));
        $longOptions = array_map(function($option) {
            return $option[1];
        }, $options);

        $foundOptions = getopt($shortOptions, $longOptions);
        $combinedOptions = array();
        foreach ($options as $key => $value) {
            $short = rtrim($value[0], ':');
            $long  = rtrim($value[1], ':');
            if (isset($foundOptions[$short])) {
                $combinedOptions[$key] = $foundOptions[$short];
            }
            elseif (isset($foundOptions[$long])) {
                $combinedOptions[$key] = $foundOptions[$long];
            }
        }

        return $combinedOptions;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
}

/**
 * Private class to help diff the two migration lists.
 */
class TypedMigration
{
    /**
     * Must be Miggy::DISK or Miggy::DB
     * @var int
     */
    private $type;

    /**
     * @var Migration
     */
    private $migration;

    /**
     * @param Migration $migration
     * @param int       $type       Must be Miggy::DISK or Miggy::DB
     */
    public function __construct(Migration $migration, $type)
    {
        $this->migration = $migration;
        $this->type = $type;
    }

    /**
     * @return Migration
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Comparator function. The standard migration comparison is used, if they match then
     * Disk comes before DB.
     *
     * @param  TypedMigration $a
     * @param  TypedMigration $b
     * @return int
     * @see    http://php.net/manual/en/function.usort.php
     */
    public static function compare(TypedMigration $a, TypedMigration $b)
    {
        $compare = MiggyMigration::compareTime($a->getMigration(), $b->getMigration());
        if ($compare === 0) {
            if ($a->getType() ===  Miggy::DISK) {
                $compare = -1;
            } else {
                $compare = 1;
            }
        }
        return $compare;
    }
}
