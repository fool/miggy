<?php

namespace fool\miggy;

/**
 * Basic properties all migrations must have
 */
interface Migration
{
    /**
     * Runs the migrations
     */
    public function up();

    /**
     * The name is a human readable and descriptive part of the migration.
     *
     * @return string
     */
    public function getName();

    /**
     * The time is a unix timestamp of when the migration was generated. Time
     * is used to order migrations. Be careful with incompatible migrations
     * created around the same time.
     *
     * It is important that every migration has a unique identifier, the name
     * and time is used as a key. There cannot be two migrations with the same
     * name and same time.
     *
     * @return int
     */
    public function getTime();

    /**
     * Are these migrations the same?
     *
     * @param  Migration $other
     * @return bool
     */
    public function equals(Migration $other);

    /**
     * @return string
     */
    public function __toString();
}
