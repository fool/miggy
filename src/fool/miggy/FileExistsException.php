<?php


namespace fool\miggy;

/**
 * Thrown when a migration file is to be created by the destination file already exists.
 */
class FileExistsException extends CodeGenerationException
{
    /**
     * @param string $file
     */
    public function __construct($file)
    {
        parent::__construct("Unable to write migration, file already exists: $file");
    }
}
