<?php

namespace fool\miggy;

use Psr\Log\LoggerInterface;

/**
 * These generate the code when you create a new migration.
 */
interface MigrationCodeGenerator
{
    /**
     * Creates a new MigrationCodeGenerator
     *
     * @param LoggerInterface $log A logger you can write any messages to during code generation
     */
    public function __construct(LoggerInterface $log);

    /**
     * @param  string  $name            The name of the migration
     * @param  string  $time            The timestamp
     * @param  string  $className       The class name
     * @param  string  $namespace       The classes namespaces (defaults to empty)
     * @param  int     $directory       The directory to write the file in
     * @throws FileExistsException
     * @throws IOException
     */
    public function writeMigrationFile($name, $time, $className, $namespace, $directory);
}
