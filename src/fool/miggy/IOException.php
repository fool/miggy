<?php

namespace fool\miggy;

/**
 * Thrown when the writing of the file fails.
 */
class IOException extends CodeGenerationException
{
    /**
     * A list of all the keys in an error_get_last() array
     * @var string[]
     */
    private static $errorGetLastKeys = array('file', 'line', 'type', 'message');

    /**
     * @param string $message      The exception message
     * @param array $errorGetLast  The result of error_get_last() after the failed write
     */
    public function __construct($message, array $errorGetLast)
    {
        if ($this->isValidErrorGetLast($errorGetLast)) {
            $message .= sprintf(" [%s:%d Error (%d): %s] %s",
                $errorGetLast['file'], $errorGetLast['line'], $errorGetLast['type'], $errorGetLast['message'], $message);
        }
        parent::__construct($message);
    }

    /**
     * Verifies the array received has all the keys an array from error_get_last() would have.
     *
     * @param  array $errorGetLast   The output of error_get_last()
     * @return bool                  True if this array looks like one that error_get_last() returns, false otherwise
     */
    public function isValidErrorGetLast(array $errorGetLast)
    {
        $keys = array_keys($errorGetLast);
        return count(array_diff(self::$errorGetLastKeys, $keys)) === 0;
    }
} 