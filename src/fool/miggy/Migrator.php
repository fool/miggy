<?php

namespace fool\miggy;

/**
 * A program that does migration related tasks.
 * Can help you create migrations, show status, and run them.
 * There is only migrate up, no down.
 *
 *
 * To use this you need to subclass Adapter. Then you can create and run the migrator executable:
 *
 *
 * $myAdapter = new MyMiggyAdapter();
 * $migrator = new Miggy($myAdapter);
 * $migrator->run();
 */
interface Migrator
{
    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter);

    /**
     * When run in CLI mode as an executable, this should print the usage of the program
     */
    public function printHelp();

    /**
     * This is intended to be used as an executable program from the command line. It
     * is just an interface to access the underlying functionality described below
     */
    public function run();


    /**
     * Find all missing migrations and run them
     *
     * @param bool $noop   True if no migrations should actually be run, just print what would have happened
     */
    public function migrateUp($noop = false);

    /**
     * Looks at every file on disk and instantiates them.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDisk();

    /**
     * Gets a sorted list of all migrations from the database with earliest first.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDatabase();

    /**
     * Lists all migrations with their statuses.
     */
    public function listAllMigrations();

    /**
     * Creates a new migration with specified name. Time can optionally be specified or defaults to current time.
     *
     * @param  string $name  The name of the new migration
     * @param  int    $time  The time this migration is created in UTC timestamp. 0 = use current time (default)
     * @return bool   True on success, False otherwise
     */
    public function create($name, $time = 0);

    /**
     * Writes the migration to the migration table - marking this as done - without running the up() function.
     * This is useful if you have an already applied migration and just need miggy to recognize it. The migration
     * can be any migration in the list that has status missing.
     *
     * @param  string $name  The classname of the migration
     * @return bool  True if the migration was skipped, False otherwise
     */
    public function skipMigration($name);

    /**
     * Runs a migration by name. up() will always start at the earliest, but sometimes you want to run a migration
     * that is not the first one.
     *
     * @param  string $name  The classname of the migration
     * @return bool   True if the migration was run, false otherwise
     */
    public function runMigration($name);
}
