<?php

namespace fool\miggy;

/**
 * Adapter is how you connect your app's specific quirks to miggy.
 *
 * @package fool\miggy
 */
interface Adapter
{
    /**
     * Creates a migration instance. This finds the subclass of Migration associated with these two unique pairs
     * and instantiates it. This does not include or require any PHP files, your class loader must be able to find
     * the file by fully qualified classname. This is to help you when you are getting all migrations in the database,
     * your table only needs to store the name and time and that will be enough to get a Migration instance.
     *
     * @param  string $name
     * @param  int    $time
     * @return Migration
     */
    public function createMigration($name, $time);

    /**
     * @return MigrationCodeGenerator
     */
    public function getMigrationCodeGenerator();

    /**
     * You need to implement a way for miggy to find your previous migrations.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDatabase();

    /**
     * Gets the path to the location on disk where migration classes will live. This is used to write
     * newly generated migrations, and discover the migrations in the codebase.
     *
     * @return string
     */
    public function getMigrationDirectory();

    /**
     * Called when a migration has been up()'d. You need to store the migration in the database.
     *
     * @param  Migration $migration
     * @return mixed
     */
    public function writeMigrationToDatabase(Migration $migration);


    /**
     * Generates a migration class name. Here's the reasoning on how the default is formed:
     * - Starts with M because all class names must begin with a letter
     * - Follows with a date time format. This helps the migrations become sortable on disk and also allows for
     *   migrations of the same name to be completed.
     * - Follows with underscore, which is one of the few allowed special characters in class name
     * - Followed by user's name for the migration
     *
     * @param  string  $name  The migration name
     * @param  int     $time  Unix timestamp for this migration
     * @return string
     */
    public function getMigrationClassName($name, $time);

    /**
     * Default: No namespace is used on auto-generated Migration classes
     *
     * @return string
     */
    public function getNamespace();

    /**
     * Called whenever miggy is shutting down. This gives you an opportunity to close your database connections
     * or clean up any state you may have initialized in the adapter. Certain events like Fatal Errors will not
     * ensure shutdown() is called.
     */
    public function shutdown();

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLog();
}