<?php


namespace fool\miggy;
use \Exception;

/**
 * All code generating errors will be defined as one of these
 */
class CodeGenerationException extends Exception
{
    /**
     * Require messages for all exceptions
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
} 