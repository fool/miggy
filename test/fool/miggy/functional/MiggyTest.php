<?php


namespace fool\test\miggy\functional;
use fool\miggy\impl\Miggy;
use fool\miggy\MigrationStatus;
use fool\test\miggy\framework\test\MiggyTestCase;
use fool\test\miggy\framework\config\TestConfig;
use fool\test\miggy\framework\miggy\TestMiggyAdapter;
use fool\test\miggy\framework\migration\MigrationMaker;
use fool\test\miggy\framework\io\Filesystem;

/**
 * function test cases. these all use real databases
 */
class MiggyTest extends MiggyTestCase
{
    public function setUp()
    {
        $data = TestConfig::getRoot() . "data";
        $ds = DIRECTORY_SEPARATOR;
        $directory = $data . $ds . "migrations" . $ds . "dataset" . TestMiggyAdapter::getDatasetCounter();
        $filesystem = new Filesystem();
        $filesystem->recursivelyDeleteDirectory($directory);
        if (!is_dir($data)) {
            mkdir($data);
        }
    }

    /**
     * Tests:
     * - Creating migrations and seeing they are found but not applied
     * - Running up() when migrations are missing, in this case all migrations were missing
     */
    public function testMiggyUpWhenAllMigrationsMissing()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;

        /* create a couple migratioms */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(2, $numberMigrations);

        /* assert that the migrations are found and have status missing */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);

        $miggy->migrateUp();

        /* assert the migrations are found and have status good */
        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(2, $numberMigrations);

        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $secondName, $secondTime, $second);

        /* assert tables exist */
        $this->assertTableExists($firstName, "First migration was run successfully but table '$firstName' does not exist");
        $this->assertTableExists($secondName, "Second migration was run successfully but table '$secondName' does not exist");
    }

    /**
     * Tests:
     * - When migrating up and multiple migrations are available only the limit number migrations are run
     */
    public function testMiggyUpWhenUsingLimit()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);

        /* migrate up 1 */
        $miggy->setLimit(1);
        $miggy->migrateUp();

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(2, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);

        /* assert table A exists but B does not */
        $this->assertTableExists($firstName, "First migration was run successfully but table '$firstName' does not exist");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does not exists");
    }

    /**
     * Stuff specific to how the code generated migrations are created. This is
     * also a test for the MiggyMigrationCreator.
     *
     * Tests:
     * - Creating a new migration writes the file to disk in the right spot
     * - There are no syntax errors in the generated migration
     * - When the file is included there are no errors
     * - Instantiating an instance of the generated migration class is spossible
     * - The generated instance is a subtype of Migration
     */
    public function testMiggyCreate()
    {
        $name = "A_Test_Migration";
        $time = mktime(10, 55, 23, 3, 15, 1996);
        $adapter = new TestMiggyAdapter();
        $class = $adapter->getMigrationClassName($name, $time);
        $miggy = new Miggy($adapter);
        $success = $miggy->create($name, $time);

        $this->assertTrue($success, "Unable to create migration");

        $destinationFile = sprintf("%s%s%s.php", $adapter->getMigrationDirectory(), DIRECTORY_SEPARATOR, $class);

        $exists = file_exists($destinationFile);
        $this->assertTrue($exists, "Generated migration file not found: $destinationFile");

        $command = "/usr/bin/env php -l " . escapeshellarg($destinationFile);
        $exitStatus = 0;
        $results = array();
        exec($command, $results, $exitStatus);
        $this->assertEquals(0, $exitStatus, "Generated migration file has syntax errors" . PHP_EOL . implode(PHP_EOL, $results));

        $success = @include_once($destinationFile);
        $this->assertEquals(1, $success, "Unable to include generated migration file");

        $namespace = $adapter->getNamespace();
        $classname = "$namespace\\$class";
        $migration = new $classname();
        $this->assertInstanceOf('\fool\miggy\Migration', $migration, "Generated migration is not a migration");
    }

    /**
     * Tests:
     * - run migration runs one migration only
     */
    public function testRunMigrationOnlyRunsOneMigration()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* run just the second migration */
        $secondClassName = $adapter->getMigrationClassName($secondName, $secondTime);
        $wasRun = $miggy->runMigration($secondClassName);
        $this->assertTrue($wasRun, "Migration was not run");

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableNotExists($firstName, "First migration was not run but table '$firstName' does exists");
        $this->assertTableExists($secondName, "Second migration was run successfully but table '$secondName' does not exist");
        $this->assertTableNotExists($thirdName, "Third migration was not run but table '$thirdName' does exists");
    }

    /**
     * Tests:
     * - When runMigration is used and the migration is not found, no migrations are run
     */
    public function testRunMigrationDoesNothingWhenMigrationNotFound()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* run a migration name that is not used */
        $wasRun = $miggy->runMigration("some fake migration name");
        $this->assertFalse($wasRun, "Migration was run");

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableNotExists($firstName, "First migration was not run but table '$firstName' does exists");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exist");
        $this->assertTableNotExists($thirdName, "Third migration was not run but table '$thirdName' does exists");
    }

    /**
     * Tests:
     * - skip only marks the migration as done, it doesnt actually run the migration
     * - all other migrations are unaffected
     */
    public function testSkipMigrationOnlyMarksMigrationAsDone()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* skip migration 2 */
        $secondClassName = $adapter->getMigrationClassName($secondName, $secondTime);
        $wasSkipped = $miggy->skipMigration($secondClassName);
        $this->assertTrue($wasSkipped, "Migration was not skipped");

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableNotExists($firstName, "First migration was not run but table '$firstName' does exists");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exist");
        $this->assertTableNotExists($thirdName, "Third migration was not run but table '$thirdName' does exists");
    }

    /**
     * Tests:
     * - If you try to skip a migration that is not found nothing is changed.
     */
    public function testSkipMigrationDoesNothingWhenMigrationNotFound()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* dont skip any migrations */
        $wasSkipped = $miggy->skipMigration("some fake migration class name");
        $this->assertFalse($wasSkipped, "Migration was skipped");

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableNotExists($firstName, "First migration was not run but table '$firstName' does exists");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exist");
        $this->assertTableNotExists($thirdName, "Third migration was not run but table '$thirdName' does exists");
    }


    /**
     * Tests:
     * - test() can find a missing migration but does nothing
     * - up() will run the missing migrations
     *
     * Database state tested:
     * Good
     * Missing
     * Good
     */
    public function testUpWhenSingleMigrationMissing()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* run just the first and third migration */
        $firstClassName = $adapter->getMigrationClassName($firstName, $firstTime);
        $wasRun = $miggy->runMigration($firstClassName);
        $this->assertTrue($wasRun, "Migration was not run");

        $thirdClassName = $adapter->getMigrationClassName($thirdName, $thirdTime);
        $wasRun = $miggy->runMigration($thirdClassName);
        $this->assertTrue($wasRun, "Migration was not run");

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exists");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");

        /* finally set up is done. run test(), it should find B but do nothing **/
        $miggy->migrateUp(true);

        /* assert table B exists but A and C do not */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exists");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");

        /* now run up() */
        $miggy->migrateUp();
        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(3, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $thirdName, $thirdTime, $third);

        /* assert table B exists but A and C do not */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableExists($secondName, "Second migration was run but table '$secondName' does not exist");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");
    }

    /**
     * Tests:
     * - If you have duplicate entries in the migration table they are caught as ERROR status
     */
    public function testUpWhenMissingMigrationAndDuplicateMigration()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $adapter->createTestMigration($secondName, $secondTime);
        $c = $adapter->createTestMigration($thirdName, $thirdTime);

        /* run just the first and third migration */
        $firstClassName = $adapter->getMigrationClassName($firstName, $firstTime);
        $wasRun = $miggy->runMigration($firstClassName);
        $this->assertTrue($wasRun, "Migration was not run");

        $thirdClassName = $adapter->getMigrationClassName($thirdName, $thirdTime);
        $wasRun = $miggy->runMigration($thirdClassName);
        $this->assertTrue($wasRun, "Migration was not run");

        /* assert table B exists but A and C do not */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' does exists");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");

        /*** now add another entry for C in the table **/
        $cClassname = $adapter->getNamespace() . '\\' . $c['class'];
        $adapter->writeMigrationToDatabase(new $cClassname);

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals(4, $numberMigrations);

        /* assert they are both found but only the first one has good status */
        $first = $migrations[0];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $firstName, $firstTime, $first);
        $second = $migrations[1];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_MISSING, $secondName, $secondTime, $second);
        $third = $migrations[2];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $thirdName, $thirdTime, $third);
        $fourth = $migrations[3];
        $this->assertMigrationStatusEqual(MigrationStatus::STATUS_ERROR, $thirdName, $thirdTime, $fourth);

        /* assert table B exists but A and C do not */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableNotExists($secondName, "Second migration was not run but table '$secondName' exists");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");
    }

    /**
     * Tests:
     * - With 50 migrations, 25 applied, and 25 missing, they are
     *   all discovered and applied appropriately
     */
    public function testUpWhenManyMigrationMissing()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $migrationsToTest = 50;
        $migrationData = array();
        $name = 'AAAA';
        $time = 100;
        for ($i = 0; $i < $migrationsToTest; $i++) {
            $migrationData[] = array($name, $time);
            $time += 100;
            $name++; /* increment on letter in PHP goes alphabetical */
        }

        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        foreach ($migrationData as $migration) {
            $adapter->createTestMigration($migration[0], $migration[1]);
        }

        $skip = false;
        foreach ($migrationData as $migration) {
            $skip = !$skip;
            if ($skip) {
                continue;
            }
            $className = $adapter->getMigrationClassName($migration[0], $migration[1]);
            $wasRun = $miggy->runMigration($className);
            $this->assertTrue($wasRun, "Migration was not run");
        }

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals($migrationsToTest, $numberMigrations);

        $skip = false;
        foreach ($migrations as $index => $migration) {
            $skip = !$skip;
            $expectedData = $migrationData[$index];
            $name = $expectedData[0];
            $time = $expectedData[1];
            $status = $skip ? MigrationStatus::STATUS_MISSING : MigrationStatus::STATUS_GOOD;
            $this->assertMigrationStatusEqual($status, $name, $time, $migration);
            if ($skip) {
                $this->assertTableNotExists($name, "Migration was not run but table '$name' exists");
            } else {
                $this->assertTableExists($name, "Migration was run but table '$name' does not exist");
            }
        }
        $miggy->migrateUp();

        $migrations =  $miggy->getDifference();
        $numberMigrations = count($migrations);
        $this->assertEquals($migrationsToTest, $numberMigrations);

        foreach ($migrations as $index => $migration) {
            $expectedData = $migrationData[$index];
            $name = $expectedData[0];
            $time = $expectedData[1];
            $this->assertMigrationStatusEqual(MigrationStatus::STATUS_GOOD, $name, $time, $migration);
            $this->assertTableExists($name, "Migration was run but table '$name' does not exist");
        }
    }

    /**
     * This results in fatal error, cant test like this
     */
    public function disabled_testMigrationMissingFromDisk()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $firstName = 'A';
        $firstTime = 100;
        $secondName = 'B';
        $secondTime = 200;
        $thirdName = 'C';
        $thirdTime = 300;

        /* create a couple migrations */
        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($firstName, $firstTime);
        $b = $adapter->createTestMigration($secondName, $secondTime);
        $adapter->createTestMigration($thirdName, $thirdTime);

        /* run just the second migration */
        $miggy->migrateUp();

        /* assert tables exist */
        $this->assertTableExists($firstName, "First migration was run but table '$firstName' does not exist");
        $this->assertTableExists($secondName, "Second migration was run but table '$secondName' does not exist");
        $this->assertTableExists($thirdName, "Third migration was run but table '$thirdName' does not exist");

        $this->assertTrue(unlink($b['file']), "Unable to delete the B migration file");

        TestConfig::getCurrentDatabase()->query("UPDATE " . TestConfig::MIGRATION_TABLE . " SET name = 'B_Renamed' WHERE name = 'B'");
        $migrations = $miggy->getDifference();
    }


    /**
     * Tests:
     * - When two or more rows in the database are duplicates they could have been matched as a good disk & db pair.
     *   Because these migrations were accepted with 'good' status, it also did not report errors when doing miggy list.
     *   This was a bug in miggy 2.1.0
     */
    public function testGetDifferenceWhenDuplicateMigrationsReportsErrors()
    {
        $adapter = new TestMiggyAdapter();
        $miggy = new Miggy($adapter);

        $name = 'Migration_Name';
        $time = 100;

        $migrationCreator = new MigrationMaker($miggy, $adapter);
        $adapter->setMigrationMaker($migrationCreator);
        $adapter->createTestMigration($name, $time);
        $miggy->migrateUp();

        /* now add the migration to the db. these two extra rows are both errors since there's no corresponding file */
        $database = TestConfig::getCurrentDatabase();
        $database->query("INSERT INTO " . TestConfig::MIGRATION_TABLE . " (name, time) VALUES ('$name', $time)");
        $database->query("INSERT INTO " . TestConfig::MIGRATION_TABLE . " (name, time) VALUES ('$name', $time)");

        $migrations = $miggy->getDifference();
        $numberMigrations = count($migrations);
        /* there is one status that encompasses the first pair of disk/db, and two extra ones we added to db */
        $this->assertEquals(3, $numberMigrations);

        /* check the first one is still legit */
        $firstMigration = array_shift($migrations);
        $this->assertEquals(MigrationStatus::STATUS_GOOD, $firstMigration->getStatus());

        /* the other 2 rows we added should be marked as duplicate */
        $this->assertCount(2, $migrations, 'There should be two migrations');
        foreach ($migrations as $migration) {
            $this->assertEquals(MigrationStatus::STATUS_ERROR, $migration->getStatus());
        }
    }
}
