<?php

namespace fool\test\miggy\framework\test;
use fool\miggy\MigrationStatus;
use fool\test\miggy\framework\config\TestConfig;
use \PHPUnit_Framework_TestCase;

/**
 * Parent for all the miggy tests
 */
abstract class MiggyTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * Helper to compare the output of getDifference()
     *
     * @param  string          $status
     * @param  string          $name
     * @param  int             $time
     * @param  MigrationStatus $migrationStatus
     * @return void
     */
    protected function assertMigrationStatusEqual($status, $name, $time, MigrationStatus $migrationStatus)
    {
        $this->assertEquals($status, $migrationStatus->getStatus(), "Migration status did not match");
        $migration = $migrationStatus->getMigration();
        $this->assertEquals($name, $migration->getName(), "Migration name did not match");
        $this->assertEquals($time, $migration->getTime(), "Migration time did not match");
    }

    /**
     * Checks if a table exists in the database
     *
     * @param  string $name
     * @return bool   True if a table with $name exists, False otherwise
     */
    private function tableExists($name)
    {
        $database = TestConfig::getCurrentDatabase();
        $query = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='$name'";
        $result = $database->query($query);
        $found = false;
        do {
            $row = $result->fetchArray();
            if ($row && $row['count(*)'] === 1) {
                $found = true;
            }
        } while ($row);
        return $found;
    }

    /**
     * @param string $name
     * @param string $message
     */
    protected function assertTableExists($name, $message)
    {
        $this->assertTrue($this->tableExists($name), $message);
    }

    /**
     * @param string $name
     * @param string $message
     */
    protected function assertTableNotExists($name, $message)
    {
        $this->assertFalse($this->tableExists($name), $message);
    }
}
