<?php

namespace fool\test\miggy\framework\migration;

use fool\miggy\Migrator;
use fool\miggy\impl\MiggyAdapter;
use fool\test\miggy\framework\io\Filesystem;
use \Exception;

/**
 * Makes migrations for you. Helps when testing.
 */
class MigrationMaker
{
    /**
     * @var Migrator
     */
    protected $miggy;

    /**
     * @var MiggyAdapter
     */
    protected $miggyAdapter;

    /**
     * @param Migrator        $miggy
     * @param MiggyAdapter    $miggyAdapter
     */
    public function __construct(Migrator $miggy, MiggyAdapter $miggyAdapter)
    {
        $this->miggy = $miggy;
        $this->miggyAdapter = $miggyAdapter;
    }

    /**
     * Creates a migration with name and at time, for tests.
     *
     * @param string $name
     * @param int    $time = 0
     * @return array (
     *  'file' =>  The file written
     *  'class' => The class name
     * )
     * @throws TestMigrationCreationException
     */
    public function makeCreateTableMigration($name, $time = 0)
    {
        if (!$this->miggy->create($name, $time)) {
            throw new TestMigrationCreationException("Miggy create failed");
        }
        $filesystem = new Filesystem();
        $directory = $this->miggyAdapter->getMigrationDirectory();
        $files = $filesystem->getFilesWithName($this->miggyAdapter->getMigrationClassName($name, $time), $directory);
        if (!$files) {
            throw new TestMigrationCreationException("Miggy create failed");
        }

        if (count($files) > 1) {
            throw new TestMigrationCreationException("too many files, confused: " . implode(', ', $files));
        }

        $file = $files[0];
        $fullPath = $directory . DIRECTORY_SEPARATOR . $file;
        $class = substr($file, 0, -4); // remove ".php"

        $migrationContents = file_get_contents($fullPath);
        if ($migrationContents === false) {
            throw new TestMigrationCreationException("unable to read file off disk: $fullPath");
        }

        $upMethodBody = "// code here bro";
        $ourCode = <<<'UPBODY'
            $query = "CREATE TABLE TBLNAMEPLACEHOLDER (
                id int primary key not null
            )";
            $this->database->exec($query);
UPBODY;
        $ourCode = str_replace('TBLNAMEPLACEHOLDER', $name, $ourCode);

        $newContents = str_replace($upMethodBody, $ourCode, $migrationContents);

        $result = file_put_contents($fullPath, $newContents);
        if ($result === false || $result !== strlen($newContents)) {
            $error = error_get_last();
            throw new TestMigrationCreationException("Unable to write file: $fullPath, " . var_export($error, true));
        }

        return array(
            'file' => $fullPath,
            'class' => $class,
        );
    }
}

/**
 * Thrown when a test migration cant be created
 */
class TestMigrationCreationException extends Exception
{
    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}