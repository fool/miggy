<?php


namespace fool\test\miggy\framework\io;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;
use \FilesystemIterator;
use \DirectoryIterator;

class Filesystem
{
    /**
     * Deletes $dir and all sub folders
     *
     * @param string $dir
     */
    public function recursivelyDeleteDirectory($dir)
    {
        if (is_dir($dir)) {
            $directoryIterator = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
            $iterator = new RecursiveIteratorIterator($directoryIterator, RecursiveIteratorIterator::CHILD_FIRST);
            foreach($iterator as $path) {
                $path->isFile() ?
                    unlink($path->getPathname())
                    : $this->recursivelyDeleteDirectory($path->getPathname());
            }
            rmdir($dir);
        }
    }

    /**
     * Finds all files in a directory that have names containing a given word
     *
     * @param  string   $name
     * @param  string   $directory
     * @return string[]  A list of the matched files, empty if none matched
     */
    public function getFilesWithName($name, $directory)
    {
        $matches = array();
        foreach (new DirectoryIterator($directory) as $fileInfo) {
            if($fileInfo->isDot()) {
                continue;
            }

            $file = $fileInfo->getFilename();
            if (strstr($file, $name)) {
                $matches[] = $file;
            }
        }
        return $matches;
    }
} 