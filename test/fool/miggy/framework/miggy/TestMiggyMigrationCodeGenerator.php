<?php

namespace fool\test\miggy\framework\miggy;

use \fool\miggy\impl\MiggyMigrationCodeGenerator;
use \Psr\Log\LoggerInterface;

class TestMiggyMigrationCodeGenerator extends MiggyMigrationCodeGenerator
{
    /**
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        parent::__construct($log);
    }

    /**
     * A list of traits that will be included in any auto-generated migration files.
     * This is an easy way to introduce code sharing across migrations. Each trait should
     * be a fully qualified class name.
     *
     * @return string[]
     */
    public function getMigrationTraits()
    {
        return array();
    }

    /**
     * A list of classes that will be included in any auto-generated migration files.
     * These are added at the top under the namespace declaration with 'use' in front of it.
     *
     * @return string[]
     */
    public function getImportedClasses()
    {
        return array();
    }


    /**
     * The class all generated migrations will inherit from
     * @return string
     */
    public function getMigrationParentClass()
    {
        return 'TestMigration';
    }

    /**
     * The namespace to import for the parent class. This can be
     * empty if the parent class is not in a namespace.
     *
     * @return string
     */
    public function getMigrationParentClassNamespace()
    {
        return '\fool\test\miggy\framework\miggy\TestMigration';
    }

} 