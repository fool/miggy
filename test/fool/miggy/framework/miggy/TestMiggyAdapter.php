<?php

namespace fool\test\miggy\framework\miggy;
use fool\echolog\Echolog;
use fool\miggy\impl\MiggyAdapter;
use fool\miggy\Migration;
use fool\test\miggy\framework\migration\MigrationMaker;
use Psr\Log\LogLevel;
use \SQLite3;
use \Exception;
use \fool\test\miggy\framework\config\TestConfig;

/**
 * The Adapter used in tests
 *
 * @package fool\test\miggy\framework\miggy
 */
class TestMiggyAdapter extends MiggyAdapter
{
    /**
     * @var SQLite3
     */
    protected $database;

    /**
     * Each test has a different number. This gives a different database file and data directory
     * @var int
     */
    private $dataset;

    /**
     * A helper to create migrations for tests
     * @var MigrationMaker
     */
    private $migrationMaker;

    /**
     * The global test counter
     * @var int
     */
    private static $datasetCounter = 1;

    /**
     * Creates an adapter that by default logs nothing
     */
    public function __construct($datasetOverride = null, $logLevel = LogLevel::EMERGENCY)
    {
        parent::__construct(new Echolog($logLevel));
        if ($datasetOverride) {
            $this->dataset = $datasetOverride;
        } else {
            $this->dataset = self::$datasetCounter++;
        }
        $this->database = $this->setupDatabase();
    }

    /**
     * @param  int $time
     * @return string
     */
    protected function serializeTime($time)
    {
        return (string) $time;
    }

    /**
     * Gets the path to the location on disk where migration classes will live. This is used to write
     * newly generated migrations, and discover the migrations in the codebase.
     *
     * @return string
     */
    public function getMigrationDirectory()
    {
        return TestConfig::getRoot() . "data/migrations/dataset{$this->dataset}";
    }

    /**
     * You need to implement a way for miggy to find your previous migrations.
     *
     * @return Migration[]
     */
    public function getAllMigrationsFromDatabase()
    {
        $migrations = array();
        $query = "SELECT name, time FROM " . TestConfig::MIGRATION_TABLE . " ORDER BY time ASC";

        $result = $this->database->query($query);
        $row = $result->fetchArray();
        if ($row) {
            do {
                $migrations[] = $this->createMigration($row['name'], $row['time']);
                $row = $result->fetchArray();
            } while ($row);
        }
        return $migrations;
    }

    /**
     * Called when a migration has been up()'d. You need to store the migration in the database.
     *
     * @param  Migration $migration
     * @return mixed
     */
    public function writeMigrationToDatabase(Migration $migration)
    {
        $query = "INSERT INTO " . TestConfig::MIGRATION_TABLE . " (name, time) VALUES (:name, :time)";
        $statement = $this->database->prepare($query);// $values = array($migration->getName(), $migration->getTime());
        $statement->bindValue(':name', $migration->getName(), SQLITE3_TEXT);
        $statement->bindValue(':time', $migration->getTime(), SQLITE3_INTEGER);


        return $statement->execute();
    }

    /**
     * @return int
     */
    public static function getDatasetCounter()
    {
        return self::$datasetCounter;
    }

    /**
     * @param  string  $name
     * @param  int     $time
     * @return array
     */
    public function createTestMigration($name, $time = 0)
    {
        return $this->migrationMaker->makeCreateTableMigration($name, $time);
    }

    /**
     * @param MigrationMaker $migrationMaker
     */
    public function setMigrationMaker(MigrationMaker $migrationMaker)
    {
        $this->migrationMaker = $migrationMaker;
    }

    /**
     * The test namespace for migrations
     * @return string
     */
    public function getNamespace()
    {
        return "fool\\data\\migrations\\dataset{$this->dataset}";
    }

    /**
     * The class that writes migrations when you call miggy --create
     *
     * @return \fool\miggy\MigrationCodeGenerator
     */
    public function getMigrationCodeGenerator()
    {
        return new TestMiggyMigrationCodeGenerator($this->log);
    }

    /**
     * Sets up a fresh db. if there is a db already it is deleted
     */
    public function setupDatabase()
    {
        $file = TestConfig::getDatabaseFile() . $this->dataset;
        if (file_exists($file)) {
            if (!@unlink($file)) {
                throw new DatabaseSetupException("Unable to delete old database: $file");
            }
        }

        $db = new SQLite3($file, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
        TestConfig::setCurrentDatabase($db);
        $db->query("

CREATE TABLE " . TestConfig::MIGRATION_TABLE . " (
    id   INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
    name varchar(255)     NOT NULL DEFAULT '',
    time INT              NOT NULL
);

");
        return $db;
    }
}

/**
 * Thrown when database setup fails
 */
class DatabaseSetupException extends Exception
{
    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}