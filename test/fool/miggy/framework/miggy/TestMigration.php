<?php


namespace fool\test\miggy\framework\miggy;


use fool\miggy\impl\MiggyMigration;
use fool\test\miggy\framework\config\TestConfig;
use \SQLite3;

/**
 * Parent for all the tests migrations. This makes it easy to get a database handle
 * inside a migration
 */
abstract class TestMigration extends MiggyMigration
{
    /**
     * @var SQLite3
     */
    protected $database;

    /**
     * @param string $name  the migration name
     * @param int    $time  unix timestamp
     */
    public function __construct($name, $time)
    {
        parent::__construct($name, $time);
        $this->database = TestConfig::getCurrentDatabase();
    }
} 