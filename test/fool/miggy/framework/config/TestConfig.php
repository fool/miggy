<?php

namespace fool\test\miggy\framework\config;
use fool\test\miggy\framework\io\Filesystem;

class TestConfig
{
    const MIGRATION_TABLE = 'migration';

    const DATABASE_FILE = 'test_database';

    /**
     * @var \SQLite3
     */
    private static $currentDatabase;

    /**
     * @var string
     */
    private static $root = '';

    /**
     * Gets the root of the miggy package, guaranteed to end in a slash
     * @return string
     */
    public static function getRoot()
    {
        if (!self::$root) {
            self::$root = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . DIRECTORY_SEPARATOR;
        }
        return self::$root;
    }

    /**
     * Removes all test data
     */
    public static function cleanTestData()
    {
        $filesystem = new Filesystem();
        $filesystem->recursivelyDeleteDirectory(TestConfig::getRoot() . 'data');
    }

    /**
     * Gets an absolute path to the database file
     * @return string
     */
    public static function getDatabaseFile()
    {
        return self::getRoot() . 'data' . DIRECTORY_SEPARATOR . self::DATABASE_FILE;
    }

    /**
     * @param \SQLite3 $currentDatabase
     */
    public static function setCurrentDatabase($currentDatabase)
    {
        self::$currentDatabase = $currentDatabase;
    }

    /**
     * @return \SQLite3
     */
    public static function getCurrentDatabase()
    {
        return self::$currentDatabase;
    }
}
