<?php

/**
 * PHPUnit bootstrap
 */

use Aura\Autoload\Loader;

class Bootstrap
{
    public static function init()
    {
        $root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
        $ds = DIRECTORY_SEPARATOR;
        $composerAutoloader = "{$root}{$ds}vendor{$ds}autoload.php";
        require_once $composerAutoloader;

        $loader = new Loader();
        $loader->addPrefix('fool\test',      "{$root}{$ds}test{$ds}fool");
        $loader->addPrefix('fool\data',      "{$root}{$ds}test{$ds}data");
        $loader->register();
    }
}

Bootstrap::init();

