<?php

$root = dirname(dirname(__FILE__));
require "{$root}/test/fool/miggy/framework/config/Bootstrap.php";
use fool\miggy\impl\Miggy;
use fool\test\miggy\framework\miggy\TestMiggyAdapter;
use Psr\Log\LogLevel;

$adapter = new TestMiggyAdapter(1, LogLevel::DEBUG);
$miggy = new Miggy($adapter);
$miggy->run();
