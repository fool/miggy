<?php

require "vendor/autoload.php";       // composer
require "ExampleMiggyAdapter.php";   // the example file

use \fool\miggy\impl\Miggy;

$exampleAdapter = new ExampleMiggyAdapter();
$miggy = new Miggy($exampleAdapter);
$miggy->run();
