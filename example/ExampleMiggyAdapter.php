<?php

use \fool\miggy\Adapter;
use \fool\miggy\impl\MiggyAdapter;
use \fool\miggy\Migration;

/**
 * This is an example of how to adapt your application to Miggy. Make sure to read the documentation for MiggyAdapter
 * because it does give you a lot of features and not all of them are used in the example.
 */
class ExampleMiggyAdapter extends MiggyAdapter implements Adapter
{
    /**
     * The name of the table that stores which migrations have been run.
     */
    const MIGRATION_TABLE = "migrations";

	/**
	 * In this example we use Propel to get a database connection
	 */
    private $database;

    /**
     * Where our migration classes live
     * @return string
     */
    public function getMigrationDirectory()
    {
        return "migrations";
    }

    /**
     * Asks the database for it's known migrations
     *
     * @return array
     */
    public function getAllMigrationsFromDatabase()
    {
        $migrations = array();
        $keepFetching = true;
        $query = "SELECT name, time FROM " . self::MIGRATION_TABLE . " ORDER BY time desc";

        $this->initializeDatabase();
        $stmt = $this->database->prepare($query);
        $stmt->execute();
        do
        {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                $migrations[] = $this->createMigration($result['name'], $result['time']);
            }
            else {
                $keepFetching = false;
            }
        } while ($keepFetching);

        return $migrations;

    }

    /**
     * The migration has already had up() called on it, now it must be written to the database to store
     * the fact that it has been run.
     *
     * @param Migration $migration
     * @return void
     */
    public function writeMigrationToDatabase(Migration $migration)
    {
        $query = "INSERT INTO " . self::MIGRATION_TABLE . " (name, time) VALUES (?, ?)";
        $values = array($migration->getName(), $migration->getTime());

        $this->initializeDatabase();
        $stmt = $this->database->prepare($query);
        $stmt->execute($values);
    }

    /**
     * Miggy will call shutdown when it is done. This allows us to close the database connection
     */
    public function shutdown()
    {
        if ($this->database) {
            Propel::close();
        }
    }


    private function initializeDatabase()
    {
        if ($this->database === null) {
            $this->database = Propel::getConnection();
        }
    }
}
